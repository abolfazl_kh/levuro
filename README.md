## Main technologies used

- [React Native](https://github.com/facebook/react-native)

> A framework for building native apps with React.

- [Redux](http://redux.js.org/)

> Redux is a predictable state container for JavaScript apps.

## Running the project

- Clone this project
```
git clone < project-url.git >
```

- Open the project directory in your terminal and run:
```
yarn install
```

### iOS steps

- Launch a virtual iOS device 

- Open ios folder of the project in your terminal and run :

```
npx pod-install
```

- Then, run the project in executing on your project folder:

```
react-native run-ios
```

### Android steps

- Launch a virtual android device

- Then, run the project in executing on your project folder:

```
react-native run-android
```






