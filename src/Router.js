import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Login from './components/screens/Login';
import UserList from './components/screens/UserList';
import InsertUser from './components/screens/InsertUser';
import Splash from './components/screens/Splash';

const AppStack = createStackNavigator();
const AppRoute = () => (
  <NavigationContainer>
    <AppStack.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <AppStack.Screen name="Splash" component={Splash} />
      <AppStack.Screen name="Login" component={Login} />
      <AppStack.Screen name="InsertUser" component={InsertUser} />
      <AppStack.Screen name="UserList" component={UserList} />
    </AppStack.Navigator>
  </NavigationContainer>
);

export default AppRoute;
