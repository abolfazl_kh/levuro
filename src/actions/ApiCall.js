import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://reqres.in/api',
  headers: {
    'Content-Type': 'application/json',
  },
});

/* Authentication */
export const login = async (email, password) => {
  try {
    const data = {email, password};
    const result = await instance.post('/login', data);

    return result.data;
  } catch (error) {
    throw new Error();
  }
};

/* Main */
export const getList = async (token, page) => {
  try {
    instance.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    const result = await instance.get(`/users?page=${page}`);

    return result.data;
  } catch (error) {
    return null;
  }
};

export const createUser = async (token, first_name, last_name, email) => {
  try {
    const body = {first_name, last_name, email};
    instance.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    const result = await instance.post(`/users/`, body);

    return result.data;
  } catch (error) {
    throw new Error();
  }
};

export const updateUser = async (
  token,
  first_name,
  last_name,
  email,
  userId,
) => {
  try {
    const body = {first_name, last_name, email};
    instance.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    const result = await instance.patch(`/users/${userId}`, body);

    return result.data;
  } catch (error) {
    throw new Error();
  }
};
