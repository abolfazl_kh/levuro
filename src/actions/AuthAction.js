import AsyncStorage from '@react-native-community/async-storage';
import {
  LOGIN_USER,
  SET_TOKEN,
  LOGOUT,
} from '../actions/types';
import * as api from './ApiCall';

export const setToken = () => async (dispatch) => {
  let token = await AsyncStorage.getItem('token');
  return dispatch({
    type: SET_TOKEN,
    token: token ? token : '',
  });
};

export const login = (email, password) => async (dispatch) => {
  const data = await api.login(email, password);
  return dispatch({
    type: LOGIN_USER,
    token: data && data.token ? data.token : '',
  });
};

export const logOut = () => {
  AsyncStorage.removeItem('token');
  return {
    type: LOGOUT,
  };
};
