import {GET_LIST, UPDATE_USER, ADD_NEW_USER,SEARCH_LIST} from '../actions/types';
import * as api from './ApiCall';

export const getList = (token, page, text) => async (dispatch) => {
  const data = await api.getList(token, page);
  const rawUserData = data && data.data ? data.data : [];
  let userData = rawUserData;
  text.length &&
    (userData = rawUserData.filter((item) => {
      const itemData = `${item.first_name.toUpperCase()} ${item.last_name.toUpperCase()} ${item.email.toUpperCase()}`;
      const searchInputData = text.toUpperCase();
      return itemData.indexOf(searchInputData) > -1;
    }));

  return dispatch({
    type: GET_LIST,
    rawUserData,
    userData,
    isDataFinisished: data && data.page >= data.total_pages ? true : false,
  });
};

export const searchList = (text, rawUserData) => {
  const userData = rawUserData.filter((item) => {
    const itemData = `${item.first_name.toUpperCase()} ${item.last_name.toUpperCase()} ${item.email.toUpperCase()}`;
    const searchInputData = text.toUpperCase();
    return itemData.indexOf(searchInputData) > -1;
  });
  return {
    type: SEARCH_LIST,
    userData,
  };
};

export const createUser = (token, first_name, last_name, email) => async (
  dispatch,
) => {
  const data = await api.createUser(token, first_name, last_name, email);
  return dispatch({
    type: ADD_NEW_USER,
    payload: data && data.data ? data.data : [],
  });
};

export const updateUser = (
  token,
  first_name,
  last_name,
  email,
  userId,
) => async (dispatch) => {
  const data = await api.updateUser(
    token,
    first_name,
    last_name,
    email,
    userId,
  );
  return dispatch({
    type: UPDATE_USER,
    payload: data && data.data ? data.data : [],
  });
};
