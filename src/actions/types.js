/* Authentication Actions */

export const SET_TOKEN = 'set_token';

export const LOGIN_USER = 'login_user';

export const LOGOUT = 'logout';

/* Main Actions */

export const GET_LIST = 'get_list';

export const SEARCH_LIST = 'search_list';

export const UPDATE_USER = 'UPDATE USER';

export const ADD_NEW_USER = 'ADD NEW USER';
