import React from 'react';
import {Text, TouchableOpacity} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const Button = ({
  buttonStyle,
  disabled,
  onPress,
  textStyle,
  children,
  outline,
}) => {
  return (
    <TouchableOpacity
      disabled={disabled}
      onPress={onPress}
      style={[
        styles.buttonStyle,
        outline && {backgroundColor: '#fff'},
        buttonStyle,
      ]}>
      <Text
        style={[styles.textStyle, textStyle, outline && {color: '#a9a9a9'}]}>
        {children}
      </Text>
    </TouchableOpacity>
  );
};
const styles = {
  textStyle: {
    alignSelf: 'center',
    fontSize: wp('4.3%'),
    color: '#fff',
    paddingVertical: hp('1.1%'),
    paddingHorizontal: wp('1.5%'),
  },
  buttonStyle: {
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: wp('3%'),
    marginVertical: hp('2%'),
    backgroundColor: '#a9a9a9',
    borderColor: '#a9a9a9',
    borderWidth: hp('0.2%'),
    borderRadius: hp('1%'),
    height: hp('6%'),
  },
};

export default Button;
