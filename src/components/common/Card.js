import React from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const Card = ({url, title, description, onPress}) => {
  return (
    <TouchableOpacity
      style={styles.cardStyle}
      onPress={onPress}
      activeOpacity={1}>
      <Image source={{uri: url}} style={styles.avatar} />

      <View>
        <Text style={[styles.textStyle, {fontWeight: 'bold'}]}>{title}</Text>
        <Text style={styles.textStyle}>{description}</Text>
      </View>
    </TouchableOpacity>
  );
};

const styles = {
  cardStyle: {
    borderWidth: wp('.6%'),
    borderRadius: wp('1%'),
    borderColor: '#a9a9a9',
    marginHorizontal: wp('2%'),
    marginVertical: hp('1%'),
    backgroundColor: '#fff',
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: wp('2%'),
    marginTop: 0,
  },
  avatar: {
    width: wp('25%'),
    height: wp('25%'),
  },
  textStyle: {
    fontSize: wp('4%'),
    fontWeight: '500',
    marginTop: hp('0.6%'),
    paddingLeft: wp('2%'),
  },
};

export default Card;
