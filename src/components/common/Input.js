import React from 'react';
import {TextInput, View, Text, Platform} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const Input = ({
  value,
  onChangeText,
  placeholder,
  secureTextEntry,
  maxLength,
  keyboardType,
}) => {
  const {inputStyle, containerSTyle} = styles;
  return (
    <View style={containerSTyle}>
      <TextInput
        secureTextEntry={secureTextEntry}
        placeholder={placeholder}
        autoCorrect={false}
        style={inputStyle}
        value={value}
        onChangeText={onChangeText}
        maxLength={maxLength}
        keyboardType={keyboardType}
      />
    </View>
  );
};

const styles = {
  inputStyle: {
    paddingHorizontal: wp('2%'),
    fontSize: wp('3.7'),
    flex: 1,
    color: 'rgba(0, 0, 0, .87)',
  },
  containerSTyle: {
    flex: 1,
  },
};

export default Input;
