import React from 'react';
import {View, ActivityIndicator, Modal} from 'react-native';

const Spinner = ({size, loading}) => {
  return (
    <Modal transparent={true} visible={loading} onRequestClose={() => {}}>
      <View style={styles.spinnerStyle}>
        <ActivityIndicator color={'#fff'} size={size || 'large'} />
      </View>
    </Modal>
  );
};

const styles = {
  spinnerStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, .8)',
  },
};
export default Spinner;
