import React, {useState} from 'react';
import {SafeAreaView, View, Text, StyleSheet} from 'react-native';
import {logOut, createUser, updateUser} from '../../actions';
import {connect} from 'react-redux';
import {UPDATE_USER} from '../../actions/types';
import Input from '../common/Input';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Button from '../common/Button';
import {DefaultTheme} from '@react-navigation/native';
import Spinner from '../common/Spinner';

function InsertUser({
  token,
  route,
  logOut,
  createUser,
  updateUser,
  navigation,
}) {
  const {type, item} = route.params;
  const [name, setName] = useState(type == UPDATE_USER ? item.first_name : '');
  const [lastName, setLastName] = useState(
    type == UPDATE_USER ? item.last_name : '',
  );
  const [email, setEmail] = useState(type == UPDATE_USER ? item.email : '');
  const [errorMessage, setErrorMessage] = useState('');
  const [loading, setLoading] = useState(false);

  const onLogout = () => {
    logOut();
    navigation.navigate('Login');
  };

  const turnOffLoading = (result) => {
    setLoading(false),
      result == 'success'
        ? navigation.navigate('UserList')
        : setErrorMessage('* Something went wrong. Please try again');
  };

  const onSubmitButton = () => {
    let emailValidator = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    emailValidator.test(email)
      ? (setLoading(true),
        type == UPDATE_USER
          ? updateUser(token, name, lastName, email, item.id)
              .then(() => turnOffLoading('success'))
              .catch(() => turnOffLoading('error'))
          : createUser(token, name, lastName, email)
              .then(() => turnOffLoading('success'))
              .catch(() => turnOffLoading('error')))
      : setErrorMessage('* Email address is not valid');
  };

  return (
    <SafeAreaView style={{flex: 1}}>
      <Spinner loading={loading} />
      <View style={styles.logoutView}>
        <Button
          outline
          buttonStyle={styles.customBackButton}
          onPress={navigation.goBack}>
          BACK
        </Button>

        <Button outline onPress={onLogout}>
          LOGOUT
        </Button>
      </View>
      <Text style={styles.warningStyle}>{errorMessage}</Text>
      <View style={styles.inputContainer}>
        <Input
          placeholder="First name"
          onChangeText={setName}
          value={name}
          maxLength={50}
        />
      </View>

      <View style={styles.inputContainer}>
        <Input
          placeholder="Last name"
          onChangeText={setLastName}
          value={lastName}
          maxLength={50}
        />
      </View>

      <View style={styles.inputContainer}>
        <Input
          placeholder="Email"
          onChangeText={setEmail}
          keyboardType={'email-address'}
          value={email}
          maxLength={50}
        />
      </View>

      <Button
        disabled={!name.length || !lastName.length || !email.length}
        onPress={onSubmitButton}>
        {type}
      </Button>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  logoutView: {flexDirection: 'row', justifyContent: 'space-between'},
  customBackButton: {
    borderWidth: 0,
    backgroundColor: DefaultTheme.colors.background,
  },
  warningStyle: {
    fontSize: wp('3.2%'),
    marginTop: hp('20%'),
    color: 'red',
    marginHorizontal: wp('3%'),
  },
  inputContainer: {
    justifyContent: 'flex-end',
    borderWidth: hp('0.2%'),
    borderColor: '#a9a9a9',
    marginTop: hp('2.2%'),
    backgroundColor: '#fff',
    borderRadius: hp('0.55%'),
    marginHorizontal: wp('3%'),
    height: hp('7%'),
  },
});

const mapStateToProps = ({auth}) => {
  const {token} = auth;
  return {token};
};

export default connect(mapStateToProps, {
  logOut,
  createUser,
  updateUser,
})(InsertUser);
