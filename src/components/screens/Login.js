import React, {useState} from 'react';
import {SafeAreaView, View, Text, StyleSheet} from 'react-native';
import {login} from '../../actions';
import {connect} from 'react-redux';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import AsyncStorage from '@react-native-community/async-storage';
import Input from '../common/Input';
import Button from '../common/Button';
import Spinner from '../common/Spinner';

const Login = (props) => {
  const {login, navigation} = props;
  const [loading, setLoading] = useState(false);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [errorMessage, setErrorMessage] = useState('');
  const onSubmitButton = () => {
    let emailValidator = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    emailValidator.test(email)
      ? (setLoading(true),
        login(email, password)
          .then(
            (res) => (
              setLoading(false),
              res.token.length
                ? (AsyncStorage.setItem('token', res.token),
                  navigation.navigate('UserList'))
                : setErrorMessage('* Something went wrong. Please try again')
            ),
          )
          .catch(
            () => (
              setLoading(false),
              setErrorMessage(
                '* The email address or password is incorrect. Please retry...',
              )
            ),
          ))
      : setErrorMessage('* Email address is not valid');
  };

  return (
    <SafeAreaView style={{flex: 1, marginTop: hp('30%')}}>
      <Spinner loading={loading} />
      <Text style={styles.warningStyle}>{errorMessage}</Text>
      <View style={styles.inputContainer}>
        <Input
          placeholder="Email"
          onChangeText={setEmail}
          value={email}
          keyboardType={'email-address'}
          maxLength={50}
        />
      </View>

      <View style={[styles.inputContainer]}>
        <Input
          secureTextEntry
          placeholder="Password"
          onChangeText={setPassword}
          value={password}
          maxLength={20}
        />
      </View>

      <Button
        disabled={!email.length || !password.length}
        onPress={onSubmitButton}>
        Login
      </Button>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  inputContainer: {
    justifyContent: 'flex-end',
    borderWidth: hp('0.2%'),
    borderColor: '#ddd',
    marginTop: hp('2.2%'),
    backgroundColor: '#fff',
    borderRadius: hp('0.55%'),
    marginHorizontal: wp('3%'),
    height: hp('7%'),
  },
  warningStyle: {
    fontSize: wp('3.2%'),
    color: 'red',
    marginTop: hp('0.85%'),
    marginHorizontal: wp('3%'),
  },
});

const mapStateToProps = ({auth}) => {
  const {email, password} = auth;
  return {email, password};
};

export default connect(mapStateToProps, {
  login,
})(Login);
