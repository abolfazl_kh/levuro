import React, {useState, useEffect} from 'react';
import {setToken} from '../../actions';
import {connect} from 'react-redux';
import Spinner from '../common/Spinner';

const Splash = ({setToken, navigation}) => {
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setTimeout(() => {
      setToken().then((res) =>
        res.token.length
          ? (navigation.navigate('UserList'), setLoading(false))
          : (navigation.navigate('Login'), setLoading(false)),
      );
    }, 1000);
  }, []);

  return (
      <Spinner loading={loading} />
  );
};

export default connect(null, {
  setToken,
})(Splash);
