import React, {useEffect, useState} from 'react';
import {SafeAreaView, View, StyleSheet, Text, ScrollView} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {getList, searchList, logOut} from '../../actions';
import {connect} from 'react-redux';
import {UPDATE_USER, ADD_NEW_USER} from '../../actions/types';
import Input from '../common/Input';
import Button from '../common/Button';
import Card from '../common/Card';
import Spinner from '../common/Spinner';

const UserList = ({
  getList,
  token,
  logOut,
  navigation,
  rawUserData,
  userData,
  isDataFinisished,
  searchList,
}) => {
  const [searchInput, setSearchInput] = useState('');
  const [page, setPage] = useState(1);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    getList(token, page, searchInput).then(() => setLoading(false));
  }, [page]);

  const onLogout = () => {
    logOut();
    navigation.navigate('Login');
  };

  const cardMap = () => {
    return userData.length ? (
      userData.map((item) => (
        <Card
          key={item.id + item.first_name}
          url={item.avatar}
          title={item.first_name + ' ' + item.last_name}
          description={item.email}
          onPress={() =>
            navigation.navigate('InsertUser', {type: UPDATE_USER, item})
          }
        />
      ))
    ) : (
      <Text style={styles.noDataText}>No Data Found!</Text>
    );
  };

  const isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
    const paddingToBottom = 20;
    return (
      layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom
    );
  };

  const onScrollLoading = (nativeEvent) => {
    !isDataFinisished && isCloseToBottom(nativeEvent) && setPage(2);
  };

  return (
    <SafeAreaView style={{flex: 1}}>
      <Spinner loading={loading} />

      <View style={styles.logoutView}>
        <Button outline onPress={onLogout}>
          LOGOUT
        </Button>
      </View>
      <Button
        onPress={() => navigation.navigate('InsertUser', {type: ADD_NEW_USER})}>
        ADD NEW USER
      </Button>
      <View style={styles.searchContainer}>
        <View style={styles.inputContainer}>
          <Input
            placeholder="search"
            onChangeText={setSearchInput}
            value={searchInput}
            maxLength={50}
          />
        </View>
        <Button
          buttonStyle={{flex: 1}}
          outline
          onPress={() => searchList(searchInput, rawUserData)}>
          SEARCH
        </Button>
      </View>

      <ScrollView
        scrollEventThrottle={16}
        onScroll={({nativeEvent}) => onScrollLoading(nativeEvent)}>
        {cardMap()}
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  logoutView: {alignItems: 'flex-end'},
  searchContainer: {flexDirection: 'row'},
  noDataText: {
    textAlign: 'center',
    color: '#000',
    fontSize: wp('4.6%'),
    fontWeight: '500',
    marginVertical: hp('0.4%'),
    justifyContent: 'flex-end',
  },
  inputContainer: {
    flex: 3,
    justifyContent: 'flex-end',
    borderWidth: hp('0.2%'),
    borderColor: '#a9a9a9',
    marginTop: hp('2.2%'),
    backgroundColor: '#fff',
    borderRadius: hp('0.55%'),
    marginHorizontal: wp('3%'),
    height: hp('6%'),
  },
});

const mapStateToProps = ({auth, main}) => {
  const {token} = auth;
  const {rawUserData, userData, isDataFinisished} = main;
  return {token, rawUserData, userData, isDataFinisished};
};

export default connect(mapStateToProps, {
  getList,
  logOut,
  searchList,
})(UserList);
