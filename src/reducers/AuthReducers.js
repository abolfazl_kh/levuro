import {SET_TOKEN} from '../actions/types';

const INITIAL_STATE = {
  token: '',
  email: '',
  password: '',
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SET_TOKEN:
      return {...state, token: action.token};

    default:
      return state;
  }
};
