import {GET_LIST, LOGOUT, SEARCH_LIST} from '../actions/types';

const INITIAL_STATE = {
  rawUserData: [],
  userData: [],
  isDataFinisished: false,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_LIST:
      return {
        ...state,
        rawUserData: state.rawUserData.concat(action.rawUserData),
        userData: state.rawUserData.concat(action.userData),
        isDataFinisished: action.isDataFinisished,
      };

    case SEARCH_LIST:
      return {...state, userData: action.userData};

    case LOGOUT:
      return {...state, rawUserData: [], isDataFinisished: false};

    default:
      return state;
  }
};
