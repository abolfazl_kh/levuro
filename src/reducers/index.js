import {combineReducers} from 'redux';
import AuthReducers from './AuthReducers';
import MainReducer from './MainReducer';
export default combineReducers({
  auth: AuthReducers,
  main: MainReducer,
});
